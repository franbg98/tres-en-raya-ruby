class TresEnRaya
    attr_reader :salir, :tabla
    def initialize

        #tablero de 3x3 casillas

        @tabla = 
        {"a1"=>" ", "a2"=>" ","a3"=>" ",
        "b1"=>" ", "b2"=>" ", "b3"=>" ",
        "c1"=>" ", "c2"=>" ", "c3"=>" "}
        
        #Posibles combinaciones ganadoras

        @columnas = [ ['a1', 'a2', 'a3'],
            ['b1', 'b2', 'b3'],
            ['c1', 'c2', 'c3'],
            ['a1', 'b1', 'c1'],
            ['a2', 'b2', 'c2'],
            ['a3', 'b3', 'c3'],
            ['a1', 'b2', 'c3'],
            ['c1', 'b2', 'a3'] ]

        @player = 'X'
        @player2 = 'O'    

        @n = 0
        @salir = false
        @mov = 0
        @m = ''
        @turno1 = false
        
        puts "Dime tu nombre player1: "
        STDOUT.flush
        @player_nombre = gets.chomp
        puts "Dime tu nombre player2: "
        STDOUT.flush
        @player2_nombre = gets.chomp

    end

    def linea
        puts "----------------------------------------------------------------------------- \n\n"
    end
    
    def dibujo_tablero
        #dibujando tablero
        puts "        a|b|c"
        puts "      1 #{@tabla["a1"]}|#{@tabla["b1"]}|#{@tabla["c1"]}"
        puts "      2 #{@tabla["a2"]}|#{@tabla["b2"]}|#{@tabla["c2"]}"
        puts "      3 #{@tabla["a3"]}|#{@tabla["b3"]}|#{@tabla["c3"]}\n\n"
    end

    def turnos
        #defino y establezco los turnos 
        comprobar_juego
        if @salir == false
            
        if @n.even? && @n < 9
            #turno player 1
            puts "#{@player_nombre} = #{@player} | #{@player2_nombre} = #{@player2} \n\n"
            print "Turno de #{@player_nombre}, introduzca su movimiento, o escriba 'salir' para finalizar: "
            puts "\n\n"
            STDOUT.flush
            @mov=gets.chomp.downcase
            if @mov == 'salir' || @n == 9
                @salir = true
            end 
            if @mov.length == 2 && @salir == false
                @m = @mov.split("")
                if(['a','b','c'].include? @m[0])
                    if(['1','2','3'].include? @m[1])
                        if @tabla[@mov] == " "
                            @tabla[@mov] = @player 
                            @n+=1
                            dibujo_tablero
                            puts "El jugador #{@player_nombre} se mueve a #{@mov}\n\n"
                            linea
                        else
                            linea
                            puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n"
                        end
                    else
                        linea
                        puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n"
                    end
                else
                    linea
                    puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n"
                end
            else
                linea
                puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n" unless @mov == 'salir'
            end
        elsif @n < 9 && @salir == false
            #turno player 2
            puts "#{@player_nombre} = #{@player} | #{@player2_nombre} = #{@player2} \n\n"
            print "Turno de #{@player2_nombre}, introduzca su movimiento, o escriba 'salir' para finalizar: "
            puts "\n\n"     
            STDOUT.flush
            @mov=gets.chomp.downcase
            if @mov == 'salir' || @n == 9
                @salir = true
            end 
                
            if @mov.length == 2
                @m = @mov.split("")
                if(['a','b','c'].include? @m[0])
                    if(['1','2','3'].include? @m[1])
                        if @tabla[@mov] == " "
                            @tabla[@mov] = @player2 
                            @n+=1
                            dibujo_tablero
                            puts "El jugador #{@player2_nombre} se mueve a #{@mov}\n\n"
                            linea
                        else
                            linea
                            puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n"
                        end
                    else
                        linea
                        puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n"
                    end
                else
                    linea
                    puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n"
                end
            else
                linea
                puts "Por favor, introduce bien el movimiento ('A1' , 'B3' , 'C2' etc).\n\n" unless @mov == 'salir'
            end
            @turno1 = false
            else
            @salir = true
            end
        end
    end

    def comprobar_juego
        #con un bucle for recorro el array de posibles combinaciones ganadoras y lo comparo con la tabla
        for j in 0..7
            trio = @columnas[j].each_slice(1).to_a
            x = 0
            o = 0
            for i in 0..2
              casilla = trio[i].join('')
              if @tabla[casilla] == "X"
                x += 1
              elsif @tabla[casilla] == "O"
                o += 1
              end
          
              if x == 3
                  @salir = true
                  puts "HA GANADO EL JUGADOR #{@player_nombre}\n\n"
                  linea
              elsif o == 3
                  @salir = true
                  puts "HA GANADO EL JUGADOR #{@player2_nombre}\n\n"
                  linea
              end
            end
        end
    end

end

juego = TresEnRaya.new

juego.dibujo_tablero

until juego.salir
   juego.linea
   juego.turnos
end


